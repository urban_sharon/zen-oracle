Zen Protocol Oracle Example 
============

An oracle service example.  
This example uses [Intrinio](http://www.intrinio.com/) as data source.  

# Prerequisites

- [Mono](http://www.mono-project.com/) installed
- a running [MongoDB](https://www.mongodb.com/) service
- a running [Node](https://gitlab.com/zenprotocol/zenprotocol)

# Build

```
./paket restore
msbuild src
```

# Configuration

The `config.yaml` file contains the following configurable items:

- `nodeApi`: API address of the Zen Node used by the oracle service
- `api`: Incoming HTTP requests endpoint
- `tickers`: A list of tickers to fetch from Intrinio and commit in the blockchain
- `interval`: In hours, the interval between data fetching requests
- `acs`: Active Contract Set settings:
    - `activate`: When activating the contract, this is the amount of blocks to have it activated for
    - `threshold`: If the contract is found to be active for less blocks than specified here, 'extend' will be triggered
    - `extend`: The amount of blocks to extend contract activation for
- `contract`: Oracle code file name
- `chain`: Specifies the chain (main, test, local)

# Wallet Setup

Set up a wallet by importing a seed:

```
zen-cli import <ZEN-WALLET-PASSWORD> feel muffin volcano click mercy abuse bachelor ginger limb tomorrow okay input spend athlete boring security document exclude liar dune usage camera ranch thought
zen-cli resync
```

Lock some funds (Zen) to it. This will be used to activate and extend the oracle contract.

# Contract Setup

1. Source
    Refer to [examples](https://github.com/zenprotocol/contracts) repository for Oracle contract source code. Make the source file available in the filesystem.
    
2. Authentication
    As the orcale contract incorporate authenticated execution, a public key needs to be extracted from the node's wallet and embedded into the body (code) of the contract.
    Use the following command to extract a public key:

    ```
    zen-cli publickey "m/44'/258'/0'/3/0" <ZEN-WALLET-PASSWORD>
    ```
    
    > The `m/44'/258'/0'/3/0` [path](https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki) is used by the oracle to perform an authenticated contract execution transaction.
    
    Then, in the source file, in line `let oraclePubKey = ""`, embed the result.
    
    For example:
    
    ```
    // The public key of the oracle  
    let oraclePubKey = "02add2eb8158d81b7ea51e35ddde9c8a95e24449bdab8391f40c5517bdb9a3e117"
    ```

3. Packing
    Using [Zebra, Zen SDK tool](https://github.com/zenprotocol/ZFS-SDK), pack the updated contract:

    ```
    zebra --pack Oracle.fst
    ```
    
    Update `config.yaml` to refer to the generaed, packed contract:
    
    ```
    contract: Zf57a7bf099b4a1d13a22c0ff5a3706297d7ca74f9aca6cc78222175030bbf997.fst
    ```
    
# Running

Use systemd to manage the deamon. See `scripts/zen-oracle.service` for environment variable configuration.

Alternatively, set the needed environment variable and launch `zen-oracle.exe`:

```
export intrinio_user_name=<INTRINIO-USERNAME>
export intrinio_password=<INTRINIO-PASSWORD>
export zen_account_password=<ZEN-WALLET-PASSWORD>
./zen-oracle.exe
```

# Wiping 

Pass the 'wipe' command line switch to wipe all commitment records from the Oracle service's database

# HTTP API

The oracle service responds to HTTP requests to the following routes:
> `<DATE>` format: `yyyy-MM-dd`
    
- `/proof?date=<DATE>&ticker=<TICKER>`

    outputs a YAML represented proof-of-inclusion of oracle data which is used to validate against the data committed to in the blockchain, for example:
    ```
    Hash: bdbac8bf395a74d45e34c04d3661ebd607dcd21a9f0eec52264b4f734ee8e288
    Index: 0
    AuditPath:
    - 6e7ce38340711d436aae480719c7c30b5bc488476bb452591b0c8dedb30149bf
    - dacf022e7b964b27eb40b45c3c0c69a6db439a09f7ea8c1f819b65f7ae7d5a7d
    - 411784e894b3d6adb3091a71d119d95fcc3c35fcb3c7239ec45e778bc86359b8
    - 2ab9b3f0f2014218623f84279fb26027a9d60504e67ffb7ba4b6fe991cf73d45
    - 776bcba2254cccaff61bc22f58f8da1a7e015dd07ec8c0d0e577d11cc14a293b
    ```

- `/data?date=<DATE>&ticker=<TICKER>`: outputs the price of TICKER for DATE
- `/data?date=<DATE>`: lists ticker-price values for DATE in Json format
- `/data?ticker=<TICKER>`: lists date-price values for TICKER in Json format

# TODO

Implement scheduling