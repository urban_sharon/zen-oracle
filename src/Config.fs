module Config

open System

let getEnv key = 
    let value = Environment.GetEnvironmentVariable(key)
    if String.IsNullOrEmpty value then
        failwith <| sprintf "missing environment variable: %s" key
    else
        value

type YamlConfig = FSharp.Configuration.YamlConfig<"config.yaml">

type Config = {
    yaml: YamlConfig
    password: string
}

let yamlConfig = new YamlConfig()
yamlConfig.Load("config.yaml")

let config = {
    yaml = yamlConfig
    password = getEnv "zen_account_password"
}

