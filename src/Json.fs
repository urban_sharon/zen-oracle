module Json

open FSharp.Data
open Api.Types
open Infrastructure
open Result
open Consensus
open Util
open Wallet
open Config

type RawResultJson = JsonProvider<"""
{
    "data": [
        {
            "identifier": "AAPL",
            "value": 208.22
        }
    ]
}""">

type AuditPathResponseJson = JsonProvider<"""
{
    "auditPath": [
        "abcd123"
    ]
}""">

let parseRawResultJson raw =
    Exception.resultWrap<RawResultJson.Root> (fun _ -> 
        RawResultJson.Parse(raw))
       "error parsing provider data"

let parseActiveContractsResponseJson raw =
    Exception.resultWrap<ActiveContractsResponseJson.Root[]> (fun _ -> 
        ActiveContractsResponseJson.Parse(raw))
       "error parsing ACS response"
    <@> Array.map (fun root -> ContractId.fromString root.ContractId, root.Expire)
    <@> Array.toList
    >>= traverseResultM (fun (contractId, exiry) -> 
        ofOption "error parsing contractid" contractId
        <@> fun contractId -> contractId, exiry
    )

let parseContractActivateResponseJson raw =
    Exception.resultWrap<ContractActivateResponseJson.Root> (fun _ -> 
        ContractActivateResponseJson.Parse(raw))
       "error parsing contract activation response"
    <@> fun root -> root.ContractId
    <@> ContractId.fromString 
    >>= ofOption "error parsing contractid of activation response"

let parseBlockChainInfoJson raw =
    Exception.resultWrap<BlockChainInfoJson.Root> (fun _ -> 
        BlockChainInfoJson.Parse(raw))
       "error parsing blockchain info response"

let contractExecuteRequestJson contractId command provideReturnAddress signingKeyDerivationPath spends messageBody =
    (new ContractExecuteRequestJson.Root(
        contractId
        |> Address.Contract
        |> Address.encode chain,
        command, 
        messageBody,
        new ContractExecuteRequestJson.Options(provideReturnAddress, signingKeyDerivationPath),
        spends,
        config.password
    )).JsonValue

let contractActivateRequestJson code numberOfBlocks =
    (new ContractActivateRequestJson.Root(code, numberOfBlocks, config.password)).JsonValue

let contractExtendRequestJson address numberOfBlocks =
    (new ContractExtendRequestJson.Root(address, numberOfBlocks, config.password)).JsonValue
