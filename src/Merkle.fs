module Merkle

open Consensus
open Hash
open Serialization

let hashLeaf timestamp (identifier, value:decimal) =
    [|
        timestamp 
        |> Timestamp.normalize 
        |> Timestamp.serialize
        
        identifier         
        |> String.serialize
        
        value 
        |> Price.normalize 
        |> Price.serialize
    |]
    |> Array.map Hash.compute
    |> Hash.joinHashes
