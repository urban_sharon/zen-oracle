module Price

let normalize : decimal -> uint64 =
    Checked.uint64
    >> Checked.(*) 1000UL