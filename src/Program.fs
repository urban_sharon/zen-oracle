﻿module Main

open System
open Result
open Consensus
open Serialization
open FsBech32
open Json
open Util
open Contract
open Config
open Timestamp

[<EntryPoint>]
let main args = 
    Server.init config.yaml.api

    if Array.exists ((=) "wipe") args then
        DataAccess.wipe() 
        |> ignore 
                
    while true do 
        result {
            do! Contract.ensureActive()
            
            let closingTime = currentClosingTime()
            
            let! data = Intrinio.fetch()
    
            let data =
                data.Data
                |> Array.map (fun root -> root.Identifier, root.Value)
    
            do! data
                |> Array.map (Merkle.hashLeaf closingTime)
                |> Array.toList
                |> MerkleTree.computeRoot
                |> Hash.bytes
                |> Zen.Types.Data.data.Hash
                |> Data.serialize
                |> Base16.encode
                |> contractExecuteRequestJson contractId "Add" false "m/44'/258'/0'/3/0" Array.empty
                |> execute "wallet/contract/execute"
                
            DataAccess.insert data closingTime
            
            info <| sprintf "data successfully persisted. %d record(s) found" (DataAccess.count())
        }
        |> mapError error
        |> ignore
            
        info "waiting..."
        
        Threading.Thread.Sleep (1000 * 60 * 60 * config.yaml.interval)
    0