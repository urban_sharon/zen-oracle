module Serialization
open System.Text
open System
open Infrastructure.Timestamp

module String =
    let serialize value = 
        ASCIIEncoding.ASCII.GetBytes (value:string)
    
    let deserialize = 
        Encoding.ASCII.GetString

let private ensureBigEndian =
    if BitConverter.IsLittleEndian then
        Array.rev
    else
        id

module private Uint32 =
    let serialize value =
        BitConverter.GetBytes (value:uint32)
        |> ensureBigEndian

    let deserialize bytes = 
        let bytes = ensureBigEndian bytes
        BitConverter.ToUInt32 (bytes,0)

module private Uint64 =
    let serialize value =
        BitConverter.GetBytes (value:uint64)
        |> ensureBigEndian

    let deserialize bytes = 
        let bytes = ensureBigEndian bytes
        BitConverter.ToUInt64 (bytes,0)

module Timestamp =
     let serialize = Uint64.serialize
     let deserialize = Uint64.deserialize
     
module Price =
     let serialize = Uint64.serialize
