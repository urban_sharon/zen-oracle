module Server

open FsNetMQ
open FSharp.Control.Reactive
open Infrastructure.Http
open Consensus
open FSharp.Data

module Actor = FsNetMQ.Actor 

let init address =     
    Actor.create (fun shim ->
        use poller = Poller.create ()
        use observer = Poller.registerEndMessage poller shim
        
        use httpAgent = Server.create poller address
        
        use observer = 
            Server.observable httpAgent |>
            Observable.subscribeWithError (fun (request,reply) ->
                let reply =
                    function
                    | None ->
                        TextContent "Not found"
                        |> reply StatusCode.NotFound 
                    | Some content -> 
                        reply StatusCode.OK content
                       
                //let (|Param|) query key = Map.tryFind key
                  
                match request with
                | Get ("/proof", query) ->
                    match Map.tryFind "date" query, Map.tryFind "ticker" query with
                    | Some date, Some ticker ->
                        Timestamp.parse date
                        |> Option.bind DataAccess.find 
                        |> Option.bind (fun data ->
                            data.data
                            |> Array.map fst
                            |> Array.tryFindIndex ((=) ticker)
                            |> Option.map (fun index ->
                                let hashes = 
                                    data.data
                                    |> Array.map (Merkle.hashLeaf data.timestamp)
                                    |> Array.toList
                                    
                                [
                                    "Time", 
                                        data.timestamp
                                        |> Timestamp.normalize
                                        |> sprintf "%iUL" 
                                    "Ticker", ticker
                                    "Price", 
                                        index
                                        |> Array.get data.data
                                        |> snd
                                        |> Price.normalize
                                        |> sprintf "%dUL"
                                    "Hash", 
                                        Merkle.hashLeaf data.timestamp (index |> Array.get data.data)
                                        |> Hash.toString
                                    "Index", 
                                        sprintf "%iul" index
                                    "AuditPath", 
                                        MerkleTree.createAuditPath hashes index
                                        |> List.map Hash.toString
                                        |> String.concat "\n - "
                                        |> sprintf "\n - %s"
                                ]
                                |> List.map (fun (key, value) -> sprintf "%s: %s" key value)
                                |> String.concat "\n"
                                |> TextContent
                            )
                        )
                    | _ -> None
                | Get ("/data", query) ->
                    match Map.tryFind "date" query, Map.tryFind "ticker" query with
                    | Some date, Some ticker ->
                        Timestamp.parse date
                        |> Option.bind DataAccess.find  
                        |> Option.map (fun data -> data.data)
                        |> Option.bind (Array.tryFind (fst >> (=) ticker))
                        |> Option.map (snd 
                                       >> JsonValue.Number 
                                       >> JsonContent)
                    | Some date, None ->
                        Timestamp.parse date
                        |> Option.bind DataAccess.find
                        |> Option.map (fun data ->
                            data.data
                            |> Array.map (fun (ticker, value) ->
                                JsonValue.Record [|
                                    "ticker", JsonValue.String ticker
                                    "value", JsonValue.Number value
                                |])
                            |> Seq.toArray
                            |> JsonValue.Array
                            |> JsonContent
                        )
                    | None, Some ticker ->
                        DataAccess.take 10
                        |> Seq.toList
                        |> Infrastructure.Option.traverseM (fun item ->
                            Array.tryFind (fst >> (=) ticker) item.data
                            |> Option.map (fun (_, value) -> item.timestamp, value)
                        )
                        |> Option.map (Seq.map (fun (timestamp, value) -> 
                            [|
                                "date", timestamp
                                |> Timestamp.toString
                                |> JsonValue.String
                                "value", JsonValue.Number value
                            |]
                            |> JsonValue.Record))
                        |> Option.map (
                            Seq.toArray
                            >> JsonValue.Array
                            >> JsonContent)
                    | _ -> None
                | _ -> None
                |> reply
            ) (fun error -> 
                printfn "error %A" error
                raise error    
            )
 
        Actor.signal shim
        Poller.run poller
    )
    |> ignore